const User = require("../models/User");

const auth = async (ws, req, next) => {
    const token = req.query.token;
    if (!token) {
        return ws.send(JSON.stringify({error: "No token presented"}));
    }
    const user = await User.findOne({token});

    if(!user) {
        return ws.send(JSON.stringify({error: "Wrong token"}));
    }
    req.user = user;
    next();
};

module.exports = auth;