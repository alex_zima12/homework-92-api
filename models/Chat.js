const mongoose = require('mongoose');
const idValidator = require('mongoose-id-validator');

const Schema = mongoose.Schema;

const ChatSchema = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: "User",
    },
    userName: {
        type: String,
        require: true
    },
    text: {
        type: String,
        require: true
    },
    datetime: {
        type: Date,
        default: Date.now
    }
});

ChatSchema.plugin(idValidator, {
    message: 'Such {PATH} doesn`t exist'
});


const Chat = mongoose.model('Chat', ChatSchema);
module.exports = Chat;