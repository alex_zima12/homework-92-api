const express = require("express");
const cors = require("cors");
const mongoose = require("mongoose");
const app = express();
require('express-ws')(app);
const port = 8000;
const chat = require('./app/chat');
const users = require("./app/users");
const config = require('./config');

app.use(cors());
app.use(express.json());
app.use(express.static("public"));

const run = async () => {
    await mongoose.connect(config.db.url + '/' + config.db.name, {useNewUrlParser: true, autoIndex: true,  useUnifiedTopology: true });

    app.use("/chat", chat);
    app.use("/users", users);

    console.log("Connected to mongo DB");

    app.listen(port, () => {
        console.log(`Server started at http://localhost:${port}`);
    });
};

run().catch(console.log);