const path = require("path");

const rootPath = __dirname;

module.exports = {
    rootPath,
    uploadPath: path.join(rootPath, "public/uploads"),
    db: {
        name: "chat",
        url: "mongodb://localhost"
    },
    fb: {
        appId: "377536106843250",
        appSecret: "7e491cfa4c4e2645f77693c05c08890c"
    }
};