const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const SALT_WORK_FACTOR = 10;
const {nanoid} = require('nanoid');

const Schema = mongoose.Schema;

const UserSchema = new Schema({
    username: {
        type: String,
        required: [true, "Field 'username' must be filled"],
        unique: true,
        validate: {
            validator: async (value) => {
                const user = await User.findOne({username: value});
                if (user) return false;
            },
            message: (props) => `User "${props.value}" already exists`
        }
    },
    email: {
        type: String,
        required: [true, "Field 'email' must be filled"],
        unique: true,
        validate: {
            validator: async (value) => {
                const user = await User.findOne({email: value});
                if (user) return false;
            },
            message: (props) => `e-mail "${props.value}" already exists`
        }
    },
    password: {
        type: String,
        required: [true, "Field 'password' must be filled"],
        minlength: [8, 'Minimal password`s length is 8 symbols'],
        validate: {
            validator: async (value) => {
                return /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$/g.test(value);
            },
            message: 'Password is too simple'
        }
    },
    token: {
        type: String,
        required: true,
    },
    role: {
        type: String,
        required: true,
        default: 'user',
        enum: ['user', 'admin']
    },
    facebookId: String,
    avatarImage: String,
});

UserSchema.pre('save', async function(next) {
    if (!this.isModified('password')) return next();
    const salt = await bcrypt.genSalt(SALT_WORK_FACTOR);
    const hash = await bcrypt.hash(this.password, salt);
    this.password = hash;
    next();
});

UserSchema.set("toJSON", {
    transform: (doc, ret) => {
        delete ret.password;
        return ret;
    }
});

UserSchema.path('email').validate(value => {
    return /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/g.test(value);
}, "Enter correct e-mail");

UserSchema.methods.checkPassword = function (password) {
    return bcrypt.compare(password, this.password);
};

UserSchema.methods.generateToken = function () {
    this.token = nanoid();
}

const User = mongoose.model('User', UserSchema);
module.exports = User;