const router = require("express").Router();
const auth = require('../middleware/auth');
const Chat = require('../models/Chat');
const User = require('../models/User');

const activeConnections = {};
const activeUsers = {};

router.ws("/", auth, async (ws, req) => {
    try {
        const token = req.query.token;
        const user = await User.findOne({token});
        const id = user._id;

        activeConnections[id] = ws;
        activeUsers[id] = user.username;

        let messages;

        await ws.on("message", async msg => {
            console.log('hello, Pasha!'); // то заходит, то нет. Проблема с асинхронностью

            const decodedMessage = JSON.parse(msg);

            switch (decodedMessage.type) {
                case "GET_ALL_MESSAGES":
                    messages = await Chat.find().populate('user').limit(30);
                    ws.send(JSON.stringify({type: "ALL_MESSAGES", messages}));
                    break;

                case "GET_ALL_USERS":
                    Object.keys(activeConnections).forEach(connId => {
                        const conn = activeConnections[connId];

                        try {
                            conn.send(JSON.stringify({
                                type: "ALL_USERS", activeUsers
                            }));
                        } catch (e) {
                            conn.send({error: "Bad Request"});
                        }
                    });
                    break;

                case "CREATE_MESSAGE":
                    Object.keys(activeConnections).forEach(connId => {
                        try {
                            const conn = activeConnections[connId];

                            const chatData = new Chat({
                                user: user._id,
                                username: decodedMessage.username,
                                text: decodedMessage.text,
                                datetime: decodedMessage.datetime
                            });

                            chatData.save();
                            conn.send(JSON.stringify({
                                type: "NEW_MESSAGE",
                                message: {
                                    user: user._id,
                                    username: user.username,
                                    text: decodedMessage.text,
                                    datetime: Date.now()
                                }
                            }));
                        } catch (e) {
                            conn.send({error: "Bad Request"});
                        }
                    });
                    break;
                default:
                    console.log("Unknown message type:", decodedMessage.type);
            }
        });

        console.log(activeUsers,"activeUsers");

        ws.on("close", async msg => {
            console.log("Client disconnected! id =", id);
            delete activeConnections[id];
            delete activeUsers[id];
        });
    } catch (e) {
        console.log(e, 'our error');
    }
});

module.exports = router;